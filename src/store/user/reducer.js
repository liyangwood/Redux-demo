import types from './constants';

const default_state = {
    username : '',
    password : '',

    isLogin : false,
    logining : false,
    loginError : null,

    profile : {
        firstName : '',
        lastName : '',
        gender : '',
        region : '',
        company : ''
    }
};

export default (state=default_state, action) => {
    switch(action.type){
        case types.UPDATE_LOGIN_INFO:
            // return Object.assign({}, state, {
            //     username: action.username,
            //     password: action.password
            // });
            return {
                ...state,
                username : action.username,
                password : action.password,
                logining : true
            };
        case types.LOGIN_SUCCESS:
            return {
                ...state,
                isLogin : true,
                logining : false,
                loginError : null,
                profile : {
                    ...state.profile,
                    ...action.profile
                }
            };
        case types.LOGIN_FAILED:
            return {
                ...state,
                isLogin : false,
                logining : false,
                loginError : action.error
            };
        case types.LOGOUT:
            return {
                ...state,
                isLogin : false,
                profile : default_state.profile
            };
        case types.UPDATE_PROFILE:
            return {
                ...state,
                profile : {
                    ...state.profile,
                    ...action.profile
                }
            };
    }

    return state;
}