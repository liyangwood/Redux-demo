import {combineReducers} from 'redux';
import {routerReducer} from 'react-router-redux';

import userReducer from './user/reducer';


const default_state = {
    name : ''
};

const appReducer = (state = default_state, action) => {
    switch (action.type) {
        
    }

    return state
}

export default combineReducers({
    app: appReducer,
    router: routerReducer,
    user: userReducer
});