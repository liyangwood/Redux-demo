import { connect } from 'react-redux';
import { withRouter } from 'react-router'
import Component from './Profile.component';
import actions from '@/store/user/actions';

const mapState = (state)=>{
    return {
        profile : state.user.profile,
        isLogin : state.user.isLogin
    };
};
const mapDispatch = (dispatch)=>{
    return {
        updateProfile(profile){
            dispatch(actions.updateProfile(profile));
        }
    };
}

export default withRouter(connect(mapState, mapDispatch)(Component));