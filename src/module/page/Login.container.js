import { connect } from 'react-redux';
import { withRouter } from 'react-router'
import Component from './Login.component';
import actions from '@/store/user/actions';

const mapState = (state)=>{
    return {
        user : state.user
    };
};
const mapDispatch = (dispatch)=>{
    return {
        login(username, password, callback){
            dispatch(actions.login(username, password)).then((flag)=>{
                flag && callback();
            });
        }
    };
}

export default withRouter(connect(mapState, mapDispatch)(Component));