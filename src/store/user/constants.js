/*
 * 定义reducer action的类型，描述action的意图
 * 
 */

export default {

    //更新username和password等login的信息
    UPDATE_LOGIN_INFO : 'USER/UPDATE_LOGIN_INFO',

    //登录
    LOGIN_SUCCESS : 'USER/LOGIN_SUCCESS',  
    LOGIN_FAILED : 'USER/LOGIN_FAILED',

    //登出
    LOGOUT : 'USER/LOGOUT',

    //更新profile
    UPDATE_PROFILE : 'USER/UPDATE_PROFILE'
    
};