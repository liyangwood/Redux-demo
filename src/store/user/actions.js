import types from './constants';
import _ from 'lodash';

export default {
    login(username, password){
        return (dispatch, getState)=>{

            // 准备开始登录
            dispatch({
                type : types.UPDATE_LOGIN_INFO,
                username,
                password
            });

            return new Promise((resolve, reject)=>{
                // 延迟处理，模拟网路请求
                _.delay(()=>{
                    // 模拟登录
                    if(username === 'hello' && password === 'world'){
                        // 登录成功
                        resolve({
                            type : types.LOGIN_SUCCESS,
                            profile : {
                                firstName : 'Jacky',
                                lastName : 'Li',
                                gender : 'male',
                                region : 'Dalian',
                                company : 'Accenture'
                            }
                        });

                    }
                    else{
                        reject({
                            type : types.LOGIN_FAILED,
                            error : '用户或密码错误'
                        });
                    }
                }, 2000);
            }).then((state)=>{
                dispatch(state);
                return true;
            }).catch((state)=>{
                dispatch(state);
                return false;
            });
        }
    },

    logout(){
        return {
            type : types.LOGOUT
        };
    },

    updateProfile(profile={}){
        return {
            type : types.UPDATE_PROFILE,
            profile
        };
    }

};