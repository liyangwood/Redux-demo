import reducer from '../reducer';
import types from '../constants';

describe('test user reducer', ()=>{
    test('should update username and password with UPDATE_LOGIN_INFO type', ()=>{
        const state = reducer(undefined, {
            type : types.UPDATE_LOGIN_INFO,
            username : 'hello',
            password : 'world'
        });

        expect(state.username).toBe('hello');
        expect(state.logining).toBe(true);
    });

    test('should login success with LOGIN_SUCCESS type', ()=>{
        const state = reducer(undefined, {
            type : types.LOGIN_SUCCESS,
            profile : {
                firstName : 'Abc',
                lastName : 'Xyz'
            }
        });

        expect(state.logining).toBe(false);
        expect(state.profile.firstName).toBe('Abc');
    });

    test('should login failed with LOGIN_FAILED type', ()=>{
        const state = reducer(undefined, {
            type : types.LOGIN_FAILED,
            error : 'error message'
        });

        expect(state.logining).toBe(false);
        expect(state.loginError).toBe('error message');
    });

    test('should clear user profile with LOGOUT type', ()=>{
        const state = reducer(undefined, {
            type : types.LOGOUT
        });

        expect(state.isLogin).toBe(false);
    });

    test('should udpate user profile with UPDATE_PROFILE type', ()=>{
        const state = reducer(undefined, {
            type : types.UPDATE_PROFILE,
            profile : {
                region : 'Dalian'
            }
        });
        expect(state.profile.region).toBe('Dalian');
    });
    
});