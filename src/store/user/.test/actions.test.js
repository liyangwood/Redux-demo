import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import actions from '../actions';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('test user actions', ()=>{
    test('should login success with correct username and password', async ()=>{
        const store = mockStore({ user: {} });
        await store.dispatch(actions.login('hello', 'world'));
        const result = store.getActions();
        
        expect(result.length).toBe(2);
        expect(result[1].profile.company).toBe('Accenture');
    });

    test('should login failed width incorrect username and password', async ()=>{
        const store = mockStore({ user: {} });
        await store.dispatch(actions.login('hello', 'xxx'));
        const result = store.getActions();

        expect(result.length).toBe(2);
        expect(result[1].error).toBe('用户或密码错误');
    });


    test('should update profile with updateProfile action', ()=>{
        const store = mockStore({ user: {} });
        store.dispatch(actions.updateProfile({
            region : 'Dalian'
        }));
        const result = store.getActions();

        expect(result[0].profile.region).toBe('Dalian');
    });

    test('should logout success', ()=>{
        const result = actions.logout();

        expect(result).not.toBeNull();
    })


    
});