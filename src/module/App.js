import React from 'react';
import { Layout } from 'antd';
import {BrowserRouter, Route, Switch} from 'react-router-dom';

import LoginPage from './page/Login.container';
import ProfilePage from './page/Profile.container';



const { Header, Footer, Sider, Content } = Layout;

export default class extends React.Component{
    render(){
        return (
            <Layout>
                <Header className="app-header"><h1>I am header</h1></Header>
                <Content id="main-page">
                    <Switch>
                        <Route path="/login" exact component={LoginPage} />
                        <Route path="/profile" exact component={ProfilePage} />

                        <Route path="/" exact component={LoginPage} />
                    </Switch>
                </Content>
            </Layout>
        );
    }
}