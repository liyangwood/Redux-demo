import React from 'react';
import ReactDOM from 'react-dom';
import _ from 'lodash';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import {Provider} from 'react-redux';
import {ConnectedRouter} from 'react-router-redux';
import store from '@/store';
import App from '@/module/App';

import '@/style/index.scss';

const render = () => {
    ReactDOM.render(
        (
            <Provider store={store}>
                <ConnectedRouter history={store.history}>
                    <App />
                </ConnectedRouter>
            </Provider>
        ),
        document.getElementById('root')
    );
};

render();