import React from 'react';

import {Form, Select, Button, Icon, Input} from 'antd';

const FormItem = Form.Item;
const Option = Select.Option;
  
class ProfileForm extends React.Component {
    handleSubmit(e){
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values);

                this.props.updateProfile(values);
            }
        });
    }

    componentWillMount(){
        if(!this.props.isLogin){
            this.props.history.replace('/login');
        }
    }
    
    render() {
        const { getFieldDecorator } = this.props.form;
        const formItemLayout = {
            labelCol: { span: 6 },
            wrapperCol: { span: 14 },
        };
        const profile = this.props.profile;
        return (
            <Form onSubmit={this.handleSubmit.bind(this)}>

                <FormItem {...formItemLayout}
                    label="First Name"
                >
                    {getFieldDecorator('firstName', {
                        initialValue : profile.firstName,
                        rules: [{ required: true, message: 'Please input your firstName' }],
                    })(
                        <Input />
                    )}
                </FormItem>
                <FormItem {...formItemLayout}
                    label="Last Name"
                >
                    {getFieldDecorator('lastName', {
                        initialValue : profile.lastName,
                        rules: [{ required: true, message: 'Please input your lastName' }],
                    })(
                        <Input />
                    )}
                </FormItem>

                <FormItem
                    {...formItemLayout}
                    label="Region"
                >
                    {getFieldDecorator('region', {
                        initialValue : profile.region,
                        rules: [
                            { required: true, message: 'Please select your region!' },
                        ],
                    })(
                        <Select placeholder="Please select a city">
                            <Option value="Dalian">Dalian</Option>
                            <Option value="Shanghai">Shanghai</Option>
                        </Select>
                    )}
                </FormItem>
                
                <FormItem {...formItemLayout}
                    label="Gender"
                >
                    <span>{profile.gender}</span>
                </FormItem>

                <FormItem {...formItemLayout}
                    label="Company"
                >
                    <span>{profile.company}</span>
                </FormItem>
          
  
                <FormItem
                    wrapperCol={{ span: 12, offset: 6 }}
                >
                    <Button type="primary" htmlType="submit">Update</Button>
                </FormItem>

                <hr/>
                <p>{profile.firstName} {profile.lastName} -- {profile.region}</p>
            </Form>
        );
    }
}

export default Form.create()(ProfileForm);