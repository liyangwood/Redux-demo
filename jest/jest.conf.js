const path = require('path');

module.exports = {
    "rootDir": path.resolve(__dirname, '../'),
    "setupFiles": [
        "<rootDir>/jest/setup.js"
    ],
    "collectCoverage": false,
    "collectCoverageFrom": [
        "src/**/*.{js,jsx}",
        "!src/*.js"
    ],
    "testMatch": [
        "**/?(*.)(spec|test).js?(x)"
    ],
    "moduleNameMapper": {
        "^@/(.*)$": "<rootDir>/src/$1"
    },
    "verbose": true
};